//Canvas.cpp
#include "Canvas.h"
#include namespace std;

Canvas::Canvas(int width, int height) : AbstractGrid(width, height), points(0) {}

void Canvas::AddShape(AbstractShape shape){
  	vector<Point> shapePoints = shape.getPoints();
  	points.insert(points.end(), shapePoints.begin(), shapePoints.end());
  	paintNeeded += shape.getPaintNeeded();
}

float Canvas::getPaintNeeded() {
   	return paintNeeded;
}

void Canvas::draw() const{
  for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            cout << getPixelAtPoint(x, y);
        }
        cout << endl;
    } 
}

string Canvas::getPixelAtPoint(int x, int y) const
{
    if (containsPoint(x, height - 1 - y)) {
        return "OO";
    } else {
        return "  ";
    }
}

bool Canvas::containsPoint(int x, int y) const
{
    for (int i = 0; i < points.size(); i++) {
        if (points[i].getX() == x && points[i].getY() == y) {
            return true;
        }
    }
    return false;
}

