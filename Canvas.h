//Canvas.h
#pragma once
#include namespace std;

class Canvas : public AbstractGrid{
  	vector<Point> points;
  	float paintNeeded;
public:
    string author;
	Canvas(int width, int height);
	~Canvas(){}
  	void AddShape(AbstractShape shape);  	
    float getPaintNeeded();
  	void draw() const;
  	string getPixelAtPoint(int x, int y) const;
  	bool containsPoint(int x, int y) const;
};
