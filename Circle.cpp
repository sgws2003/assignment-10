//Circle.cpp
#include "Circle.h"
#include namespace std;

Circle::Circle(Point center, int radius){
  	this->center = center;
    this->radius = radius;

    // calculate points of circle using the midpoint circle algorithm
    int x = 0;
    int y = radius;
    int d = 1 - radius;
    while (x <= y) {
        pointsOfShape.push_back(Point(center.getX() + x, center.getY() + y));
        pointsOfShape.push_back(Point(center.getX() - x, center.getY() + y));
        pointsOfShape.push_back(Point(center.getX() + x, center.getY() - y));
        pointsOfShape.push_back(Point(center.getX() - x, center.getY() - y));
        pointsOfShape.push_back(Point(center.getX() + y, center.getY() + x));
        pointsOfShape.push_back(Point(center.getX() - y, center.getY() + x));
        pointsOfShape.push_back(Point(center.getX() + y, center.getY() - x));
        pointsOfShape.push_back(Point(center.getX() - y, center.getY() - x));
        if (d < 0) {
            d += 2 * x + 3;
        } else {
            d += 2 * (x - y) + 5;
            y--;
        }
        x++;
    }
}

float Circle::getPaintNeeded(){
  
  	// you need to return (x2 - x1) * (y2 - y1);
  
	int x2 = center.getX();
	int x1; //= radius.getX();
	int y2 = center.getY();
	int y1; //= radius.getY();
  	
  	return (x2 - x1) * (y2 - y1);
}


