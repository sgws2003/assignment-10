//Line.cpp
#include "Line.h"
#include namespace std;

Line::Line(Point startPoint, Point endPoint){
  	this->startPoint = startPoint;
    this->endPoint = endPoint;

    // calculate slope and y-intercept of line
    int dx = endPoint.getX() - startPoint.getX();
    int dy = endPoint.getY() - startPoint.getY();

    if (dx == 0) { // vertical line
        int yStart = min(startPoint.getY(), endPoint.getY());
        int yEnd = max(startPoint.getY(), endPoint.getY());
        for (int y = yStart; y <= yEnd; y++) {
            pointsOfShape.push_back(Point(startPoint.getX(), y));
        }
    } else if (dy == 0) { // horizontal line
        int xStart = min(startPoint.getX(), endPoint.getX());
        int xEnd = max(startPoint.getX(), endPoint.getX());
        for (int x = xStart; x <= xEnd; x++) {
            pointsOfShape.push_back(Point(x, startPoint.getY()));
        }
    } else { // diagonal line
        float slope = (float) dy / dx;
        float yIntercept = startPoint.getY() - slope * startPoint.getX();
        int xStart = min(startPoint.getX(), endPoint.getX());
        int xEnd = max(startPoint.getX(), endPoint.getX());
        for (int x = xStart; x <= xEnd; x++) {
            int y = round(slope * x + yIntercept);
            pointsOfShape.push_back(Point(x, y));
        }
    }
}

float Line::getPaintNeeded(){
  
  	// you need to return (x2 - x1) * (y2 - y1);
  
	int x2; //= startpoint.getX();
	int x1; //= endpoint.getX();
	int y2; //= startpoint.getY();
	int y1; //= endpoint.getY();
  	
  	return (x2 - x1) * (y2 - y1);
}

