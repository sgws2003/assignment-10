//Line.h
#pragma once
#include namespace std;

class Line : public AbstractShape {
  	Point startPoint;
  	Point endPoint;
public:
  	Line(Point startPoint, Point endPoint);
  	float getPaintNeeded();
};
