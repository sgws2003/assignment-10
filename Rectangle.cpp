//Rectangle.cpp
#include "Rectangle.h"
#include namespace std;

Rectangle::Rectangle(Point bottomLeft, Point upperRight) : AbstractShape(){
    this->bottomLeft = bottomLeft;
    this->upperRight = upperRight;
  	// BOTTOM LINE  
    for(int i = bottomLeft.getX(); i <= upperRight.getX(); i++){
      	pointsOfShape.push_back(Point(i, bottomLeft.getY()));
    }
   	// TOP LINE  
    for(int i = bottomLeft.getX(); i <= upperRight.getX(); i++){
      	pointsOfShape.push_back(Point(i, upperRight.getY()));
    }
  	// LEFT LINE
  	for(int i = bottomLeft.getY(); i <= upperRight.getY(); i++){
      	pointsOfShape.push_back(Point(bottomLeft.getX(), i));
    }
  	// RIGHT LINE
  	for(int i = bottomLeft.getY(); i <= upperRight.getY(); i++){
      	pointsOfShape.push_back(Point(upperRight.getX(), i));
    }
};
float Rectangle::getPaintNeeded(){
  	// you need to return (x2 - x1) * (y2 - y1);
	int x2 = upperRight.getX();
	int x1 = bottomLeft.getX();
	int y2 = upperRight.getY();
	int y1 = bottomLeft.getY();
  	return (x2 - x1) * (y2 - y1);
};
