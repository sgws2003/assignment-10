//Circle.h
#pragma once
#include namespace std;

class Circle : public AbstractShape {
  	Point center;
  	int radius;
public:
  	Circle(Point center, int radius);
  	float getPaintNeeded();
};
