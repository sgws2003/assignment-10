//AbstractGrid.cpp
#include "AbstractGrid.h"
#include namespace std;

AbstractGrid::AbstractGrid(){
    this->width = 0;
    this->height = 0;
}

AbstractGrid::AbstractGrid(int width, int height)
{
    this->width = width;
    this->height = height;
}
