//AbstractGrid.h
#pragma once
#include namespace std;

class AbstractGrid {
protected:
    int width;
    int height;
public:
    AbstractGrid();
    AbstractGrid(int width, int height);
    virtual void draw() const = 0;
};
