//main.cpp
#include "Point.h"
#include "Canvas.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Line.h"
#include namespace std;

int main(){
  
  	int CANVAS_SIZE_X = 30;
  	int CANVAS_SIZE_Y = 30;
  	int CANVAS_MAX_X = CANVAS_SIZE_X - 1;
  	int CANVAS_MAX_Y = CANVAS_SIZE_Y - 1;
  
  	Canvas canvas(CANVAS_SIZE_X, CANVAS_SIZE_Y); // "points" IS EMPTY
  
  	AbstractShape rect = Rectangle(Point(0, 0), Point(CANVAS_MAX_X, CANVAS_MAX_Y)); // We set "pointsOfShape" in the contructor of the Box class
  	canvas.AddShape(rect); // We add the box to the canvas, by copyint "pointsOfShape" inside the shape to "points" inside the Canvas
  
  	AbstractShape line1 = Line(Point(1, CANVAS_MAX_X-1), Point(CANVAS_MAX_X-1, 1));
  	canvas.AddShape(line1);
  	
  	AbstractShape line2 = Line(Point(1, 1), Point(CANVAS_MAX_X, CANVAS_MAX_X-1));
  	canvas.AddShape(line2);
  
  	AbstractShape circle=Circle(Point(CANVAS_MAX_X/2,CANVAS_MAX_Y/2), CANVAS_MAX_X/2);
    canvas.AddShape(circle);
  
    canvas.draw();
  
  	
  	return 0;
}
