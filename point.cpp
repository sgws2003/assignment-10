//Point.cpp
#include "Point.h"
#include namespace std;

Point::Point(){
    this->x = 0;
    this->y = 0;
}

Point::Point(const Point& p){
    this->x = p.x;
    this->y = p.y;
}

Point::Point(int x, int y)
{
    this->x = x;
    this->y = y;
}

int Point::getX() const { return x; }

int Point::getY() const { return y; }

