//Point.h
#pragma once
#include namespace std;

class Point {
    int x;
    int y;
public:
    Point();
    Point(const Point& p);
    Point(int x, int y);
    int getX() const;
    int getY() const;
};
