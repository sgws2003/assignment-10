//AbstractShape.cpp
#include "AbstractShape.h"
#include namespace std;

AbstractShape::AbstractShape(): pointsOfShape(0) { }

const std::vector<Point> AbstractShape::getPoints() const { return pointsOfShape; }

float AbstractShape::getPaintNeeded() const { return 0.0; }
