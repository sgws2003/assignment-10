//AbstractShape.h
  #pragma once
#include "Point.h"
  #include namespace std;

class AbstractShape {
protected:
    vector<Point> pointsOfShape;
public:
    AbstractShape();
    const std::vector<Point> getPoints() const;
    virtual float getPaintNeeded() const;
};
